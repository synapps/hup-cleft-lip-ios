//
//  CLSurgicalPointCell.h
//  CHOPCleftLip
//
//  Created by Risheek on 10/9/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLSurgicalPoint.h"

@class CLSurgicalPointCell;

@protocol CLSurgicalPointCellDelegate

@required

@end

@interface CLSurgicalPointCell : UITableViewCell

@property (strong, nonatomic) CLSurgicalPoint *surgicalPoint;

@property (weak, nonatomic) id <CLSurgicalPointCellDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;

@end
