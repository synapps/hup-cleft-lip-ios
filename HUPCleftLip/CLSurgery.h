//
//  CLSurgery.h
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLSurgery : NSObject

@property int identity;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSArray *surgicalPoints;

- (CLSurgery *) initWithIdentity:(NSInteger)identity
                            name:(NSString *)name
                  surgicalPoints:(NSArray *)surgicalPoints;

@end
