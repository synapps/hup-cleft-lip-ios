//
//  CLIntroViewController.m
//  CHOPCleftLip
//
//  Created by Risheek on 12/28/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLIntroViewController.h"

@interface CLIntroViewController ()

@property (strong, nonatomic) NSArray *experienceOptions;

@end

@implementation CLIntroViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.experienceOptions = [NSArray arrayWithObjects:@"MS1-3",@"MS4",@"R1",@"R2",@"R3",@"R4",@"R5",@"R6",@"R7+",@"Fellow", @"Faculty", nil];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *name = [defaults objectForKey:@"name"];
    if(name) {
        
        [self.continueButton setTitle:[@"Continue As " stringByAppendingString:name] forState:UIControlStateNormal];
        
    } else {
        
        self.continueButton.hidden = true;
        
    }
    
    self.nameField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0,15,self.nameField.frame.size.height)];
    self.nameField.leftViewMode = UITextFieldViewModeAlways;
    self.emailField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0,15,self.nameField.frame.size.height)];
    self.emailField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)done:(id)sender {
    
    if (sender == self.continueButton) {
    
        [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
        
    } else {
        if (self.nameField.text.length > 0 && [self isEmailGood:self.emailField.text] && ![self.experienceButton.titleLabel.text  isEqual: @"Choose Medical Experience"]) {
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:self.nameField.text forKey:@"name"];
            [defaults setObject:self.emailField.text forKey:@"email"];
            [defaults setObject:self.experienceButton.titleLabel.text forKey:@"experience"];
            
            [self.presentingViewController dismissViewControllerAnimated:true completion:nil];
        } else {
            
            UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Incomplete Form" message:@"Please enter your email and name. Then, choose a level of medical experience. If you would like to continue with previous settings, select the button at the bottom of the page." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [av show];
            
        }
    }
    
}

-(BOOL) isEmailGood:(NSString *)tempMail
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:tempMail];
}

- (IBAction)showExperienceOptions:(id)sender {
    
    UIActionSheet *experienceOptionsSheet = [[UIActionSheet alloc] initWithTitle:@"Medical Experience" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles: nil];
    for (NSString *s in self.experienceOptions) {
        [experienceOptionsSheet addButtonWithTitle:s];
    }
    [experienceOptionsSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex>0) {
    
        [self.experienceButton setTitle:self.experienceOptions[buttonIndex-1] forState:UIControlStateNormal];
    }
    
}

- (IBAction)nameField:(id)sender {
}
@end
