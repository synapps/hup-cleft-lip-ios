//
//  CLSurgicalPoint.m
//  CHOPCleftLip
//
//  Created by Risheek on 10/9/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLSurgicalPoint.h"

@implementation CLSurgicalPoint

- (CGFloat)x {
    return self.point.x;
}

- (CGFloat)y {
    return self.point.y;
}

- (CLSurgicalPoint *)initWithIdentity:(NSInteger)identity name:(NSString *)name point:(CGPoint)point information:(NSString *)information {
    
    self.identity = identity;
    self.name = name;
    self.point = CGPointMake(point.x, point.y);
    self.information = information;
    return [super init];
    
}

- (NSString *)description {
    return [NSString stringWithFormat: @"Name: %@, x: %f, y: %f, info: %@", self.name, self.x, self.y, self.information];

}

@end
