//
//  CLSurgeryCell.h
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLSurgery.h"

@class CLSurgeryCell;

@protocol CLSurgeryCellDelegate

@required
- (void)learnSurgeryAtCell:(CLSurgeryCell *)cell;
- (void)testSurgeryAtCell:(CLSurgeryCell *)cell;

@end

@interface CLSurgeryCell : UITableViewCell

@property (strong, nonatomic) CLSurgery *surgery;

@property (weak, nonatomic) id <CLSurgeryCellDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UIButton *learnButton;
@property (strong, nonatomic) IBOutlet UIButton *testButton;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *nameLabelYAlignmentConstraint;

@end