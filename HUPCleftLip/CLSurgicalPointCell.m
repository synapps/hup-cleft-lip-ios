//
//  CLSurgicalPointCell.m
//  CHOPCleftLip
//
//  Created by Risheek on 10/9/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLSurgicalPointCell.h"

@implementation CLSurgicalPointCell

- (void)setSurgicalPoint:(CLSurgicalPoint *)surgicalPoint{
    
    self.nameLabel.text = surgicalPoint.name;
    
    _surgicalPoint = surgicalPoint;
}

- (void)awakeFromNib
{
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.contentView.backgroundColor = [UIColor darkGrayColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if (selected) {
        self.contentView.backgroundColor = [UIColor lightGrayColor];
        
    } else {
        
        self.contentView.backgroundColor = [UIColor darkGrayColor];
        
    }
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    
    [super setEditing:editing animated:animated];
    
    self.backgroundView = [[UIView alloc] init];
    self.backgroundView.backgroundColor = [UIColor darkGrayColor];
    
}

@end
