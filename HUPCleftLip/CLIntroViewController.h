//
//  CLIntroViewController.h
//  CHOPCleftLip
//
//  Created by Risheek on 12/28/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLIntroViewController : UIViewController <UIActionSheetDelegate>
@property (strong, nonatomic) IBOutlet UIButton *experienceButton;
@property (strong, nonatomic) IBOutlet UIButton *continueButton;
@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;

- (IBAction)done:(id)sender;
- (IBAction)showExperienceOptions:(id)sender;

@end
