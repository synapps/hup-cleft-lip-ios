//
//  CLMasterViewController.h
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLSurgeryCell.h"

@class CLDetailViewController;

@interface CLMasterViewController : UITableViewController <CLSurgeryCellDelegate>

@property (strong, nonatomic) CLDetailViewController *detailViewController;

- (void)learnSurgeryAtCell:(CLSurgeryCell *)cell;

- (void)testSurgeryAtCell:(CLSurgeryCell *)cell;

@end
