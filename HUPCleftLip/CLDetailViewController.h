//
//  CLDetailViewController.h
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLSurgery.h"

#import "CLSurgicalPointCell.h"

@interface CLDetailViewController : UIViewController <UISplitViewControllerDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate>

@property (strong, nonatomic) CLSurgery *surgery;

@property (nonatomic) NSInteger surgicalPointIndex;

@property (nonatomic) NSInteger mode; //learn: 0, test: 1

@property (nonatomic) double accuracyScore;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *surgeryImageView;
@property (strong, nonatomic) IBOutlet UIImageView *rulerImageView;
@property (strong, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (strong, nonatomic) IBOutlet UILabel *chooseSurgeryLabel;
@property (strong, nonatomic) IBOutlet UIImageView *chopIconImageView;
@property (strong, nonatomic) IBOutlet UIButton *rulerToggleButton;
@property (strong, nonatomic) IBOutlet UIWebView *webView;

- (void)didTouchImageView:(UITapGestureRecognizer *)recognizer;
- (void)didRotateRuler:(UIRotationGestureRecognizer *)recognizer;
- (void)didMoveRuler:(UIPanGestureRecognizer *)recognizer;
- (IBAction)toggleRuler:(UIButton *)sender;
- (CGPoint)surgicalPointPosition;

@end