//
//  CLSurgicalPoint.h
//  CHOPCleftLip
//
//  Created by Risheek on 10/9/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLSurgicalPoint : NSObject

@property int identity;
@property (strong, nonatomic) NSString *name;
@property CGPoint point;
@property (readonly) CGFloat x;
@property (readonly) CGFloat y;
@property NSString *information;

- (CLSurgicalPoint *) initWithIdentity:(NSInteger)identity
                                  name:(NSString *)name
                                 point:(CGPoint)point
                           information:(NSString *)information;

@end
