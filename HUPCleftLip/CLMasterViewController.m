//
//  CLMasterViewController.m
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLMasterViewController.h"

#import "CLSurgeryViewController.h"

#import "CLDetailViewController.h"

#import "CLSurgery.h"

#import "CLSurgicalPoint.h"

#import "CLSurgeryCell.h"

@interface CLMasterViewController ()

@property (strong, nonatomic) NSArray *surgeries;

@end

@implementation CLMasterViewController

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.preferredContentSize = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    //self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    //UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
    //self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (CLDetailViewController *)[self.splitViewController.viewControllers lastObject];
    //self.detailViewController.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,self.detailViewController.view.frame.size.width,self.detailViewController.view.frame.size.height)];
    //self.detailViewController.webView.hidden = true;
    if (!self.surgeries) {
        self.surgeries = [[NSArray alloc] initWithObjects:
                          [[CLSurgery alloc] initWithIdentity:0 name:@"Unilateral Millard" surgicalPoints:[[NSArray alloc] initWithObjects:
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:0 name:@"Midline columellar base" point:CGPointMake(591, 345) information:@"Paragraph 1"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:1 name:@"Noncleft columellar base" point:CGPointMake(559, 328) information:@"Paragraph 2"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:2 name:@"Cleft columellar base" point:CGPointMake(628, 345) information:@"Paragraph 3"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:3 name:@"Noncleft alar base" point:CGPointMake(425, 349) information:@"Paragraph 4"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:4 name:@"Cleft alar base" point:CGPointMake(1010, 408) information:@"Paragraph 5"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:5 name:@"Superior corner of medial lip incision" point:CGPointMake(620, 375) information:@"Paragraph 6"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut into noncleft side beneath columellar base" point:CGPointMake(558, 353) information:@"Paragraph 7"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Midline Cupid’s bow" point:CGPointMake(542, 542) information:@"Paragraph 8"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noncleft Cupid’s bow peak" point:CGPointMake(478, 578) information:@"Paragraph 9"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Cleft Cupid’s bow peak (measured)" point:CGPointMake(597, 500) information:@"Paragraph 10"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noncleft Cupid’s bow peak" point:CGPointMake(519, 631) information:@"Paragraph 11"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Cleft Cupid’s bow peak" point:CGPointMake(624, 536) information:@"Paragraph 12"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of M-flap" point:CGPointMake(652, 566) information:@"Paragraph 13"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut for interposition triangle" point:CGPointMake(594, 569) information:@"Paragraph 14"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of C-flap" point:CGPointMake(691, 372) information:@"Paragraph 15"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of M-flap" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noordhoff’s point" point:CGPointMake(933, 664) information:@"Paragraph 17"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superomedial corner of lateral skin flap" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noordhoff’s point" point:CGPointMake(885, 706) information:@"Paragraph 19"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Anterior corner vermillion interposition triangle flap" point:CGPointMake(903, 694) information:@"Paragraph 20"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Posterior corner vermillion interposition triangle flap" point:CGPointMake(866, 724) information:@"Paragraph 21"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Medial corner vermillion interposition triangle flap" point:CGPointMake(864, 680) information:@"Paragraph 22"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of L-flap" point:CGPointMake(838, 746) information:@"Paragraph 23"],
                                                                                                           [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of L-flap" point:CGPointMake(831, 549) information:@"Paragraph 24"],
                                                                                                           nil]],
                          
                                        [[CLSurgery alloc] initWithIdentity:1 name:@"Unilateral Mohler" surgicalPoints:[[NSArray alloc] initWithObjects:
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:0 name:@"Midline columellar base" point:CGPointMake(591, 345) information:@"Paragraph 1"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:1 name:@"Noncleft columellar base" point:CGPointMake(559, 328) information:@"Paragraph 2"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:2 name:@"Cleft columellar base" point:CGPointMake(628, 345) information:@"Paragraph 3"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:3 name:@"Noncleft alar base" point:CGPointMake(425, 349) information:@"Paragraph 4"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:4 name:@"Cleft alar base" point:CGPointMake(1010, 408) information:@"Paragraph 5"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:5 name:@"Superior corner of medial lip incision" point:CGPointMake(620, 375) information:@"Paragraph 6"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut into noncleft side of columellar base" point:CGPointMake(558, 353) information:@"Paragraph 7"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Midline Cupid’s bow" point:CGPointMake(542, 542) information:@"Paragraph 8"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noncleft Cupid’s bow peak" point:CGPointMake(478, 578) information:@"Paragraph 9"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Cleft Cupid’s bow peak (measured)" point:CGPointMake(597, 500) information:@"Paragraph 10"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noncleft Cupid’s bow peak" point:CGPointMake(519, 631) information:@"Paragraph 11"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Cleft Cupid’s bow peak" point:CGPointMake(624, 536) information:@"Paragraph 12"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of M-flap" point:CGPointMake(652, 566) information:@"Paragraph 13"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut for interposition triangle" point:CGPointMake(594, 569) information:@"Paragraph 14"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of C-flap" point:CGPointMake(691, 372) information:@"Paragraph 15"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of M-flap" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superior aspect of cutaneous roll, noncleft side" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superior aspect of cutaneous roll, cleft side" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noordhoff’s point" point:CGPointMake(933, 664) information:@"Paragraph 17"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Top of skin interpositional triangle" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Medial corner of skin interpositional triangle" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superomedial corner of lateral skin flap" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noordhoff’s point" point:CGPointMake(885, 706) information:@"Paragraph 19"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Anterior corner vermillion interposition triangle flap" point:CGPointMake(903, 694) information:@"Paragraph 20"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Posterior corner vermillion interposition triangle flap" point:CGPointMake(866, 724) information:@"Paragraph 21"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Medial corner vermillion interposition triangle flap" point:CGPointMake(864, 680) information:@"Paragraph 22"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of L-flap" point:CGPointMake(838, 746) information:@"Paragraph 23"],
                                                                                                                   [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of L-flap" point:CGPointMake(831, 549) information:@"Paragraph 24"],
                                                                                                                   nil]],
                          [[CLSurgery alloc] initWithIdentity:2 name:@"Unilateral Fisher" surgicalPoints:[[NSArray alloc] initWithObjects:
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:0 name:@"Midline columellar base" point:CGPointMake(591, 345) information:@"Paragraph 1"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:1 name:@"Noncleft columellar base" point:CGPointMake(559, 328) information:@"Paragraph 2"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:2 name:@"Cleft columellar base" point:CGPointMake(628, 345) information:@"Paragraph 3"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:3 name:@"Noncleft subalare" point:CGPointMake(425, 349) information:@"Paragraph 4"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:4 name:@"Cleft subalare" point:CGPointMake(1010, 408) information:@"Paragraph 5"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:5 name:@"Noncleft nasal sill" point:CGPointMake(620, 375) information:@"Paragraph 6"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Cleft nasal sill" point:CGPointMake(558, 353) information:@"Paragraph 7"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Midline Cupid’s bow" point:CGPointMake(542, 542) information:@"Paragraph 8"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noncleft Cupid’s bow peak" point:CGPointMake(478, 578) information:@"Paragraph 9"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Cleft Cupid’s bow peak (measured)" point:CGPointMake(597, 500) information:@"Paragraph 10"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noncleft Cupid’s bow peak" point:CGPointMake(519, 631) information:@"Paragraph 11"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Cleft Cupid’s bow peak" point:CGPointMake(624, 536) information:@"Paragraph 12"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of M-flap" point:CGPointMake(652, 566) information:@"Paragraph 13"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut for interposition triangle" point:CGPointMake(594, 569) information:@"Paragraph 14"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of C-flap" point:CGPointMake(691, 372) information:@"Paragraph 15"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of M-flap" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noordhoff’s point" point:CGPointMake(933, 664) information:@"Paragraph 17"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superomedial corner of lateral skin flap" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noordhoff’s point" point:CGPointMake(885, 706) information:@"Paragraph 19"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Anterior corner vermillion interposition triangle flap" point:CGPointMake(903, 694) information:@"Paragraph 20"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Posterior corner vermillion interposition triangle flap" point:CGPointMake(866, 724) information:@"Paragraph 21"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Medial corner vermillion interposition triangle flap" point:CGPointMake(864, 680) information:@"Paragraph 22"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of L-flap" point:CGPointMake(838, 746) information:@"Paragraph 23"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of L-flap" point:CGPointMake(831, 549) information:@"Paragraph 24"],
                                                                                                          nil]],

                          [[CLSurgery alloc] initWithIdentity:3 name:@"Bilateral Millard/Mulliken" surgicalPoints:[[NSArray alloc] initWithObjects:
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:0 name:@"Midline columellar base" point:CGPointMake(591, 345) information:@"Paragraph 1"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:1 name:@"Noncleft columellar base" point:CGPointMake(559, 328) information:@"Paragraph 2"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:2 name:@"Cleft columellar base" point:CGPointMake(628, 345) information:@"Paragraph 3"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:3 name:@"Noncleft alar base" point:CGPointMake(425, 349) information:@"Paragraph 4"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:4 name:@"Noncleft alar nasal sill" point:CGPointMake(1010, 408) information:@"Paragraph 5"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:5 name:@"Cleft alar base" point:CGPointMake(620, 375) information:@"Paragraph 6"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Cleft alar nasal sill" point:CGPointMake(558, 353) information:@"Paragraph 7"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Midline Cupid’s bow" point:CGPointMake(542, 542) information:@"Paragraph 8"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noncleft Cupid’s bow peak" point:CGPointMake(478, 578) information:@"Paragraph 9"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noncleft Cupid’s bow peak" point:CGPointMake(519, 631) information:@"Paragraph 11"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Cleft Cupid’s bow peak" point:CGPointMake(624, 536) information:@"Paragraph 12"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of M-flap" point:CGPointMake(652, 566) information:@"Paragraph 13"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Backcut for interposition triangle" point:CGPointMake(594, 569) information:@"Paragraph 14"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of C-flap" point:CGPointMake(691, 372) information:@"Paragraph 15"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of M-flap" point:CGPointMake(727, 416) information:@"Paragraph 16"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Noordhoff’s point" point:CGPointMake(933, 664) information:@"Paragraph 17"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Superomedial corner of lateral skin flap" point:CGPointMake(894, 500) information:@"Paragraph 18"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Vermillion wet-dry junction at Noordhoff’s point" point:CGPointMake(885, 706) information:@"Paragraph 19"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Anterior corner vermillion interposition triangle flap" point:CGPointMake(903, 694) information:@"Paragraph 20"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Posterior corner vermillion interposition triangle flap" point:CGPointMake(866, 724) information:@"Paragraph 21"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Medial corner vermillion interposition triangle flap" point:CGPointMake(864, 680) information:@"Paragraph 22"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Distal corner of L-flap" point:CGPointMake(838, 746) information:@"Paragraph 23"],
                                                                                                          [[CLSurgicalPoint alloc] initWithIdentity:6 name:@"Base of L-flap" point:CGPointMake(831, 549) information:@"Paragraph 24"],
                                                                                                          nil]],
                          nil];
    }
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CLSurgeryCell" bundle: [NSBundle bundleForClass:[CLSurgeryCell class]]] forCellReuseIdentifier:@"surgeryCell"];
    self.tableView.backgroundColor = [UIColor darkGrayColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 - (void)insertNewObject:(id)sender
 {
 if (!_objects) {
 _objects = [[NSMutableArray alloc] init];
 }
 [_objects insertObject:[NSDate date] atIndex:0];
 NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
 [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 }
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqual: @"masterSurgery"]) {
        CLSurgeryViewController *svc = segue.destinationViewController;
        svc.detailViewController = self.detailViewController;
    }
    
}

#pragma mark - Table View

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (int)(700.0 / self.surgeries.count + 1);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.surgeries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLSurgeryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"surgeryCell" forIndexPath:indexPath];
    
    cell.delegate = self;
    cell.surgery = self.surgeries[indexPath.row];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

/*
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 [self.surgeries removeObjectAtIndex:indexPath.row];
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView.indexPathForSelectedRow != nil && self.tableView.indexPathForSelectedRow.row==indexPath.row) {
        
        // Deselect manually.
        [self tableView:tableView willDeselectRowAtIndexPath:indexPath];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self tableView:tableView didDeselectRowAtIndexPath:indexPath];
        
        return nil;
    }
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //CLSurgery *surgery = self.surgeries[indexPath.row];
    //self.detailViewController.surgery = surgery;
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return indexPath;
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
}

#pragma mark - Surgery cell delegate

- (void)learnSurgeryAtCell:(CLSurgeryCell *)cell {
    self.detailViewController.webView.hidden = false;
    
    int surgeryID = cell.surgery.identity;
    if(surgeryID==0)
        [self.detailViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    else if(surgeryID==1)
        [self.detailViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    else if(surgeryID==2)
        [self.detailViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    else if(surgeryID==3)
        [self.detailViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    else
        [self.detailViewController.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]]];
    
}

- (void)testSurgeryAtCell:(CLSurgeryCell *)cell {
    
    self.detailViewController.webView.hidden = true;
    
    self.detailViewController.surgery = cell.surgery;
    self.detailViewController.mode = 1;
    [[[[UIApplication sharedApplication] delegate]window] makeKeyAndVisible];
    [self performSegueWithIdentifier:@"masterSurgery" sender:self];
    
}


@end
