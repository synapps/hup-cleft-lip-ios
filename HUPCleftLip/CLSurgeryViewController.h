//
//  CLSurgeryViewController.h
//  CHOPCleftLip
//
//  Created by Risheek on 10/8/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLDetailViewController.h"

#import "CLSurgicalPointCell.h"

@interface CLSurgeryViewController : UITableViewController <CLSurgicalPointCellDelegate>

@property (strong, nonatomic) CLDetailViewController *detailViewController;

- (void)nextPoint:(id)sender;

@end
