//
//  CLDetailViewController.m
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLDetailViewController.h"

@interface CLDetailViewController ()

@property (strong, nonatomic) UIPopoverController *masterPopoverController;

@property (strong, nonatomic) NSMutableArray *dotLayers;
@property (strong, nonatomic) NSMutableArray *lineLayers;

@property (strong, nonatomic) NSLayoutConstraint *surgeryImageViewWidthConstraint;

@property (strong, nonatomic) NSLayoutConstraint *surgeryImageViewHeightConstraint;

@property BOOL constraintsAdded;

@property (nonatomic) CGRect defaultRulerFrame;

- (void)updateDotLayers;

- (void)configureView;

- (CGRect) visibleRect;

@end

@implementation CLDetailViewController

#pragma mark - Managing the detail item

- (void)setSurgery:(CLSurgery *)surgery {
    
        _surgery = surgery;
        
        // Update the view.
        [self updateDotLayers];
        [self configureView];
    
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}

- (void)setSurgicalPointIndex:(NSInteger)surgicalPointIndex {
    
        _surgicalPointIndex = surgicalPointIndex;
        
        // Update the view.
        [self configureView];
    
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
}

- (void)setMode:(NSInteger)mode {
    
    
        
        _mode = mode;
        
        // Update the view.
        [self updateDotLayers];
        [self configureView];
        
    
    
    if (self.masterPopoverController != nil) {
        [self.masterPopoverController dismissPopoverAnimated:YES];
    }
    
}

- (double)accuracyScore {
    
    double score = 0.0;
    int count = 0;
    
    for(int i = 0; i < self.surgery.surgicalPoints.count; i++) {
        
        if(i < self.surgicalPointIndex || ((i == self.surgicalPointIndex || self.surgicalPointIndex==-1) && ((CAShapeLayer *)self.dotLayers[i]).position.x != -20)) {
            
            CGPoint a = ((CAShapeLayer *)self.dotLayers[i]).position;
            CGPoint b = ((CLSurgicalPoint *)self.surgery.surgicalPoints[i]).point;
            double dx = a.x - b.x;
            double dy = a.y - b.y;
            score += sqrt( dx*dx + dy*dy );
            
        }
        
        count++;
        
    }
    
    return score / count;
}

- (void)updateDotLayers {
    
    self.scrollView.minimumZoomScale = 1;
    self.scrollView.maximumZoomScale = 10;
    self.scrollView.zoomScale = 1;
    
    _surgicalPointIndex = -1;
    self.dotLayers = [NSMutableArray array];
    self.lineLayers = [NSMutableArray array];
    [self.surgeryImageView layer].sublayers = [NSArray array];
    
    for (CLSurgicalPoint *point in self.surgery.surgicalPoints) {
        
        CAShapeLayer *layer = [CAShapeLayer layer];
        
        if (self.mode == 0)
            layer.position = CGPointMake(point.x, point.y);
        else
            layer.position = CGPointMake(-20, -20);
        
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0.0, 0.0, 8.0, 8.0)];
        
        layer.path = path.CGPath;
        layer.lineWidth = 1.0;
        
        if(self.dotLayers.count > 0) {
            CAShapeLayer *lineLayer = [CAShapeLayer layer];
            lineLayer.strokeColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
            lineLayer.fillColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
            lineLayer.lineWidth = 5.0;
            [self.lineLayers addObject:lineLayer];
            [[self.surgeryImageView layer] addSublayer:lineLayer];
        }
        
        [self.dotLayers addObject:layer];
        [[self.surgeryImageView layer] addSublayer:layer];
        
    }
    
}

- (void)configureView
{
    // Update the user interface for the detail item.
    
    
    
    
    //self.scrollView.contentSize = self.view.bounds.size;
    //self.surgeryImageView.frame = self.view.bounds;
    
    self.surgeryImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.rulerImageView.contentMode = UIViewContentModeScaleAspectFill;
    //self.scrollView.contentSize = self.surgeryImageView.bounds.size;
    
    if (self.surgery) {
        self.surgeryImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"Surgery%d", self.surgery.identity]];
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Surgery%d", self.surgery.identity]];
        self.welcomeLabel.hidden = true;
        self.chooseSurgeryLabel.hidden = true;
        self.chopIconImageView.hidden = true;
        self.rulerToggleButton.hidden = false;
        self.rulerImageView.hidden = true;
        //self.rulerImageView.frame = CGRectMake(self.rulerImageView.frame.origin.x, self.rulerImageView.frame.origin.y, self.rulerImageView.image.size.width, self.rulerImageView.image.size.height);
        
        if (self.mode == 0) {
            
            for(int i = 0; i < self.surgery.surgicalPoints.count; i++) {
                
                CAShapeLayer *layer = ((CAShapeLayer *)self.dotLayers[i]);
                
                if(i < self.surgicalPointIndex) {
                    
                    layer.hidden = false;
                    layer.fillColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
                    layer.strokeColor = [UIColor whiteColor].CGColor;
                    
                } else if (i == self.surgicalPointIndex) {
                    
                    layer.hidden = false;
                    layer.fillColor = [UIColor whiteColor].CGColor;
                    layer.strokeColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
                    
                } else {
                    
                    layer.hidden = true;
                    
                }
                
            }
            
        } else if (self.mode == 1) {
            
            for(int i = 0; i < self.surgery.surgicalPoints.count; i++) {
                
                CAShapeLayer *layer = ((CAShapeLayer *)self.dotLayers[i]);
                layer.hidden = false;
                if(i == self.surgicalPointIndex) {
                    
                    layer.fillColor = [UIColor whiteColor].CGColor;
                    layer.strokeColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
                    
                } else {
                    
                    layer.fillColor = [UIColor colorWithRed:6.0/255 green:62.0/255 blue:177.0/255 alpha:1.0].CGColor;
                    layer.strokeColor = [UIColor whiteColor].CGColor;
                    
                }
            }
        }
    } else {
        
        self.welcomeLabel.hidden = false;
        self.chooseSurgeryLabel.hidden = false;
        self.chopIconImageView.hidden = false;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.surgeryImageView.userInteractionEnabled = YES;
    
    
    [self.surgeryImageView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchImageView:)]];
    
    UIGestureRecognizer *gestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(didRotateRuler:)];
    gestureRecognizer.delegate = self;
    [self.rulerImageView addGestureRecognizer:gestureRecognizer];
    
    gestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didMoveRuler:)];
    gestureRecognizer.delegate = self;
    [self.rulerImageView addGestureRecognizer:gestureRecognizer];
    
    
    
    //self.scrollView
    [self configureView];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return self.containerView;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)didTouchImageView:(UITapGestureRecognizer *)recognizer {
    
    if (self.mode == 1) {
        if(self.surgicalPointIndex > -1) {
            
            CGPoint point = [recognizer locationOfTouch:(recognizer.numberOfTouches - 1) inView:recognizer.view];
            ((CAShapeLayer *)self.dotLayers[self.surgicalPointIndex]).position = CGPointMake(point.x - 8.0, point.y - 8.0);
            if(self.surgicalPointIndex>0) {
                
                
                CAShapeLayer *lineLayer = self.lineLayers[self.surgicalPointIndex-1];
               
                lineLayer.hidden = true;
               
                UIBezierPath *linePath = [UIBezierPath bezierPath];
                CGPoint previousPoint = ((CAShapeLayer *)self.dotLayers[self.surgicalPointIndex-1]).position;
                previousPoint = CGPointMake(previousPoint.x + 4.0, previousPoint.y+4.0);
                point = CGPointMake(point.x - 4.0, point.y-4.0);
                [linePath moveToPoint:previousPoint];
                [linePath addLineToPoint:point];
                lineLayer.path = [linePath CGPath];
                
                lineLayer.hidden = false;
                
            }
            
        }
    }
    
    
}

- (void)didRotateRuler:(UIRotationGestureRecognizer *)recognizer {
    
    NSLog(@"rotating...");
    
    if (recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateChanged) {
        
        if (recognizer.state == UIGestureRecognizerStateBegan) {
            UIView *view = [recognizer view];
            CGPoint location = [recognizer locationInView:view];
            CGPoint anchorPoint = CGPointMake(location.x / view.bounds.size.width, location.y / view.bounds.size.height);
            
            
            CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x,
                                           view.bounds.size.height * anchorPoint.y);
            CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x,
                                           view.bounds.size.height * view.layer.anchorPoint.y);
            
            newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
            oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);
            
            CGPoint position = view.layer.position;
            
            position.x -= oldPoint.x;
            position.x += newPoint.x;
            
            position.y -= oldPoint.y;
            position.y += newPoint.y;
            
            view.layer.position = position;
            view.layer.anchorPoint = anchorPoint;
            /*
            CGPoint location = [recognizer locationInView:recognizer.view];
            recognizer.view.layer.anchorPoint = CGPointMake(location.x / recognizer.view.bounds.size.width, location.y / recognizer.view.bounds.size.height);
            recognizer.view.center = [recognizer locationInView:recognizer.view.superview];
             */
        }
        
        [recognizer.view setTransform:CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation)];
        recognizer.rotation = 0;
    }
    
}

- (void)didMoveRuler:(UIPanGestureRecognizer *)recognizer {
    
    NSLog(@"moving...");
    
    if (recognizer.state == UIGestureRecognizerStateBegan || recognizer.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [recognizer translationInView:recognizer.view];
        [recognizer.view setTransform:CGAffineTransformTranslate(recognizer.view.transform, translation.x, translation.y)];
        [recognizer setTranslation:CGPointZero inView:recognizer.view];
    }
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
    
}

- (IBAction)toggleRuler:(UIButton *)sender {
    
    
    if(self.rulerImageView.hidden) {
        UIView *superview = [self.rulerImageView superview];
        self.rulerImageView.transform = CGAffineTransformIdentity;
        CGRect visibleRect = [self visibleRect];//[self.scrollView convertRect:self.scrollView.bounds toView:self.surgeryImageView];
        if(CGRectIsEmpty(self.defaultRulerFrame))
            self.defaultRulerFrame = self.rulerImageView.frame;
        [self.rulerImageView removeFromSuperview];
        self.rulerImageView.translatesAutoresizingMaskIntoConstraints = YES;
        [self.rulerImageView setFrame:CGRectMake(visibleRect.origin.x + 30,visibleRect.origin.y + 20, self.rulerImageView.frame.size.width, self.rulerImageView.frame.size.height)];
        [superview addSubview:self.rulerImageView];
        //self.rulerImageView.transform = CGAffineTransformIdentity;
        
        //self.rulerImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 300 + visibleRect.origin.x - self.defaultRulerFrame.origin.x, visibleRect.origin.y + visibleRect.size.height - self.defaultRulerFrame.origin.y - 50);
        
        //self.rulerImageView.frame = CGRectMake(visibleRect.origin.x + 60, visibleRect.origin.y + visibleRect.size.height / 2, currentFrame.size.width, currentFrame.size.height);
        //self.rulerImageView.frame = CGRectMake(visibleRect.origin.x + visibleRect.size.width / 2 - rulerSize.width / 2, visibleRect.origin.y + visibleRect.size.height / 2 - rulerSize.height / 2, rulerSize.width, rulerSize.height);
        //self.rulerImageView.transform = CGAffineTransformIdentity;
        //self.rulerImageView.transform = CGAffineTransformScale(CGAffineTransformIdentity, self.scrollView.zoomScale, self.scrollView.zoomScale);
        self.rulerImageView.hidden = false;
        self.scrollView.scrollEnabled = false;
        self.scrollView.maximumZoomScale = self.scrollView.zoomScale;
        self.scrollView.minimumZoomScale = self.scrollView.zoomScale;
    } else {
        self.rulerImageView.hidden = true;
        self.scrollView.scrollEnabled = true;
        self.scrollView.maximumZoomScale = 10;
        self.scrollView.minimumZoomScale = 1;
    }
    
}

- (CGRect) visibleRect{
    CGRect visibleRect;
    
    visibleRect.origin = self.scrollView.contentOffset;
    visibleRect.size = self.scrollView.bounds.size;
    
    visibleRect.origin.x /= self.scrollView.zoomScale;
    visibleRect.origin.y /= self.scrollView.zoomScale;
    visibleRect.size.width /= self.scrollView.zoomScale;
    visibleRect.size.height /= self.scrollView.zoomScale;
    return visibleRect;
}

- (CGPoint)surgicalPointPosition {
    
    return ((CAShapeLayer *)self.dotLayers[self.surgicalPointIndex]).position;
    
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    barButtonItem.title = NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.masterPopoverController = nil;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation {
    return NO;
}
@end
