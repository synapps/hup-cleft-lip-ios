//
//  CLSurgeryCell.m
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLSurgeryCell.h"

@interface CLSurgeryCell()

- (void)learnButtonSelected:(UIButton *)sender;
- (void)testButtonSelected:(UIButton *)sender;

@end

@implementation CLSurgeryCell

- (void)setSurgery:(CLSurgery *)surgery {
    
    self.nameLabel.text = surgery.name;
    
    _surgery = surgery;
}

- (void)awakeFromNib
{
    // Initialization code
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.learnButton addTarget:self action:@selector(learnButtonSelected:) forControlEvents:UIControlEventTouchUpInside];
    [self.testButton addTarget:self action:@selector(testButtonSelected:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    
    [super setSelected:selected animated:animated];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    
    
    
    // Configure the view for the selected state
    if(selected) {
        
        self.learnButton.alpha = 1.0;
        self.testButton.alpha = 1.0;
        self.nameLabelYAlignmentConstraint.constant = 30;
        
    } else {
        
        self.learnButton.alpha = 0.0;
        self.testButton.alpha = 0.0;
        self.nameLabelYAlignmentConstraint.constant = 0;
        
    }
    
    
    [self layoutIfNeeded];
    [UIView commitAnimations];
    
    
}


- (void)learnButtonSelected:(UIButton *)sender {
    
    [self.delegate learnSurgeryAtCell:self];
    
}

- (void)testButtonSelected:(UIButton *)sender {
    
    [self.delegate testSurgeryAtCell:self];
    
}

@end
