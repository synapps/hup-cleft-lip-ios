//
//  CLSurgeryViewController.m
//  CHOPCleftLip
//
//  Created by Risheek on 10/8/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLSurgeryViewController.h"

#import "CLSurgicalPoint.h"

#import "CLSurgicalPointCell.h"

@interface CLSurgeryViewController ()

@property (strong, nonatomic) NSMutableArray *surgicalPoints;

@property int reloadSelection;

@end

@implementation CLSurgeryViewController

- (void)setDetailViewController:(CLDetailViewController *)detailViewController {
    
    
    _detailViewController = detailViewController;
    
    // Update the view.
    [self configureView];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CLSurgicalPointCell" bundle: [NSBundle bundleForClass:[CLSurgicalPoint class]]] forCellReuseIdentifier:@"surgericalPointCell"];
    self.tableView.backgroundColor = [UIColor lightGrayColor];
    
    self.surgicalPoints = self.detailViewController.surgery.surgicalPoints.mutableCopy;
    
    if(self.detailViewController.mode==1) {
        //[self.surgicalPoints sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
    }
    
    
    [self configureView];
}

- (void)configureView {
    
    self.title = self.detailViewController.surgery.name;
    
    [self.tableView reloadData];
    
    if (self.detailViewController.mode == 1) {
        //[self.tableView setEditing:true animated:true];
        //[((UIButton *)[self.tableView footerViewForSection:0]) setTitle:@"Continue" forState:UIControlStateNormal];
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //return (int)((700.0 - [self tableView:tableView heightForFooterInSection:indexPath.section] - [self tableView:tableView heightForHeaderInSection:indexPath.section]) / self.surgicalPoints.count + 1);
    return 75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 100;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if(self.detailViewController.mode == 0) {
        
        return 100;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.detailViewController.surgery.surgicalPoints.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLSurgicalPointCell *cell = [tableView dequeueReusableCellWithIdentifier:@"surgericalPointCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.delegate = self;
    cell.surgicalPoint = self.surgicalPoints[indexPath.row];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */


 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
     
     CLSurgicalPoint *movingSurgicalPoint = self.surgicalPoints[fromIndexPath.row];
     [self.surgicalPoints removeObjectAtIndex:fromIndexPath.row];
     [self.surgicalPoints insertObject:movingSurgicalPoint atIndex:toIndexPath.row];
     
 }
 

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.detailViewController.mode == 1)
        return nil;
    
    return indexPath;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.detailViewController.surgicalPointIndex = (indexPath ? indexPath.row : -1);
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return indexPath;
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if(self.detailViewController.mode == 0) {
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    
    button.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, [self tableView:tableView heightForHeaderInSection:section]);
    
    button.backgroundColor = [UIColor darkGrayColor];
    
    
    
        if([self.tableView indexPathForSelectedRow]) {
            [button setTitle:((CLSurgicalPoint *)self.surgicalPoints[self.reloadSelection]).information forState:UIControlStateNormal];
        } else {
            [button setTitle:@"Press the button at the bottom to continue." forState:UIControlStateNormal];
        }
        return button;
        
    } else {
        return nil;
    }
    
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
        
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeSystem];
    nextButton.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, [self tableView:tableView heightForFooterInSection:section]);
    nextButton.backgroundColor = [UIColor darkGrayColor];
    
    [nextButton setTitle:@"Next Point" forState:UIControlStateNormal];
    nextButton.titleLabel.font = [nextButton.titleLabel.font fontWithSize:20.0];
    [nextButton addTarget:self action:@selector(nextPoint:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.detailViewController.mode==1 && [self.detailViewController accuracyScore]!=0) {
        if(self.reloadSelection == -1) {
            [nextButton setTitle:[NSString stringWithFormat:@"Average Deviation: %f", [self.detailViewController accuracyScore]] forState:UIControlStateNormal];
            nextButton.userInteractionEnabled = false;
            nextButton.backgroundColor = [UIColor lightGrayColor];
            [self submitTest];
        } else if (self.reloadSelection     == self.surgicalPoints.count - 1) {
            [nextButton setTitle:@"Submit Test" forState:UIControlStateNormal];
        }
    }
    
    return nextButton;
    
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleNone;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)nextPoint:(id)sender {
    
    if (self.tableView.editing) {
        
        [self.tableView setEditing:false animated:false];
    }
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    
    if (indexPath==nil) {
        if (self.detailViewController.mode==0 || [self.detailViewController accuracyScore]==0) {
            indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            
        }
        
        
    } else if (self.detailViewController.mode == 1 && self.detailViewController.surgicalPointPosition.x == -20) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Invalid Answer"
                                   message:@"You must select a point on the image to continue testing."
                                  delegate:nil
                         cancelButtonTitle:@"OK"
                         otherButtonTitles:nil];
        [alert show];
        return;
    } else if (indexPath.row == self.surgicalPoints.count - 1) {
        
        
        indexPath = [self tableView:self.tableView willDeselectRowAtIndexPath:indexPath];
        
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        [self tableView:self.tableView didDeselectRowAtIndexPath:indexPath];
        
        indexPath = nil;
        
        
    } else {
        
        indexPath = [NSIndexPath indexPathForRow:(indexPath.row + 1) inSection:indexPath.section];

    }
    
    //indexPath = [self tableView:self.tableView willSelectRowAtIndexPath:indexPath];
    
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
    
    self.reloadSelection = indexPath ? indexPath.row : -1;
    
    [self.tableView reloadData];
    
    [self.tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
    [self tableView:self.tableView didSelectRowAtIndexPath:indexPath];
}

- (void)submitTest {
    
    if(self.detailViewController.mode == 1) {
        [self.detailViewController.scrollView setZoomScale:1.0];
        // Create the request.
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://appsbysynapps.com/HUPApp2/cleftadded.php"]];
        
        // Specify that it will be a POST request
        request.HTTPMethod = @"POST";
        
        // This is how we set header fields
        //[request setValue:@"application/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        
        int mode = self.detailViewController.surgery.identity;
        double avg_dist = [self.detailViewController accuracyScore];
        int seq_correct = 0;
        for(int i = 0; i < [self.surgicalPoints count]; i++) {
            if(self.surgicalPoints[i]==self.detailViewController.surgery.surgicalPoints[i])
                seq_correct++;
        }
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setInteger:[defaults integerForKey:@"count"]+1 forKey:@"count"];

        
        NSString *med_experience = [defaults objectForKey:@"experience"];
        NSString *email = [defaults objectForKey:@"email"];
        
        
        // Convert your data and set your request's HTTPBody property
        NSString *stringData = [NSString stringWithFormat:@"mode=%d&avg_dist=%f&seq_correct=%d&med_experience=%@&email=%@&submit=%@", mode, avg_dist, seq_correct, med_experience, email, @"Send"];
        NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        request.HTTPBody = requestBodyData;
        
        // Create url connection and fire request
        NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
}


@end
