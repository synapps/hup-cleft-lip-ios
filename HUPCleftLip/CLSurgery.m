//
//  CLSurgery.m
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import "CLSurgery.h"

@implementation CLSurgery

- (CLSurgery *)initWithIdentity:(NSInteger)identity name:(NSString *)name surgicalPoints:(NSArray *)surgicalPoints {
    
    self.identity = identity;
    self.name = name;
    self.surgicalPoints = surgicalPoints;
    return [super init];
    
}

- (NSString *)description {
    
    return [NSString stringWithFormat: @"ID: %d,Name: %@, Surgical Points: %@", self.identity, self.name, self.surgicalPoints.description];
    
}

@end
