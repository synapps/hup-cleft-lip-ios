//
//  CLAppDelegate.h
//  CHOPCleftLip
//
//  Created by Risheek on 9/27/14.
//  Copyright (c) 2014 Synapps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
